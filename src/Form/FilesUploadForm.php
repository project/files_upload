<?php

namespace Drupal\files_upload\Form;

use Drupal\file\Entity\File;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implementing Class FilesUploadForm.
 *
 * @package Drupal\files_upload\Form
 *   File upload.
 */
class FilesUploadForm extends FormBase {
  Use StringTranslationTrait;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs InviteByEmail .
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * Summary of getFormId
   * @return string
   */
  public function getFormId() {
    return 'message_time_settings_form';
  }

  /**
   * Summary of buildForm
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['files_upload'] = [
      '#type' => 'details',
      '#title' => $this->t('Files Upload'),
      '#open' => TRUE,
    ];

    $form['files_upload']['image_dir'] = [
      '#title' => $this->t('Files'),
      '#type' => 'managed_file',
      '#upload_location' => 'public://',
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#description' => $this->t('Allowed all type extensions.'),
      '#upload_validators' => [
        'file_validate_extensions' => [],
        'file_validate_size' => 200000000,
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#default_value' => $this->t('Upload'),
    ];

    return $form;
  }

  /**
   * Summary of validateForm
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Summary of submitForm
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fids = $form_state->getValue(['files_upload' => 'image_dir']);
    $total = count($fids);
    $batch = [
      'title' => $this->t('Files Upload...'),
      'operations' => [],
      'init_message' => $this->t('Files upload process is starting.'),
      'progress_message' => $this->t('Processed @current out of @total. Estimated time: @estimate.'),
      'error_message' => $this->t('The process has encountered an error.'),
    ];
    foreach ($fids as $fid) {
      $batch['operations'][] = [[
        '\Drupal\files_upload\Form\FilesUploadForm',
        'filesUploadBatchProcess',
      ],
      [$fid],
      ];
    }
    batch_set($batch);
    $this->messenger()->addMessage($total . ' Files Uploaded.');

  }

  /**
   * Implementing filesUploadBatchProcess.
   */
  public static function filesUploadBatchProcess($fid, &$context) {
    $message = 'Files Upload...';
    $results = [];
    $file = File::load($fid);
    $file->getFilename();
    $file->setPermanent();
    $file->save();
    $context['message'] = $message;
    $context['results'] = $results;
  }

}
