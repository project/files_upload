# Bulk Files Upload

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## Introduction

Bulk Files Upload allows users to upload files as many they want through admin interface. Its allow all file extension.

- For a full description of the module, visit the
  [project page](https://www.drupal.org/project/files_upload).
- To submit bug reports and feature suggestions, or to track changes, see the
  [issue queue](https://www.drupal.org/project/issues/files_upload).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
1. Configure the bulk files upload at Administration > content > files.
1. Click on file upload to add files as many you want.


## Maintainers

Current maintainers:

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
- Radheshyam Kumawat (radheymkumar) - https://www.drupal.org/u/radheymkumar
